#include"nameGenerator.h"
#include<ctime>
#include<cstdlib>
#include<string>

using std::string;
void randActivate();

const int numberOfNames = 10;
const int minimumAge = 18;
const int maximumAge = 75;

string maleNames[numberOfNames] = { "John","Andrew","Robert","Joseph","William","Anthony","Alexander","Jacob","Michael","Samuel" };
string femaleNames[numberOfNames] = { "Amantha","Jessica","Rachael","Monica","Samantha","Joanna","Christina","Catherina","Diana","Laura" };
string surnames[numberOfNames] = { "Lorentz","Smith","Sanchez","Black","White","Hopkins","Hendrixen","Wellington","Joaster","Carriz" };
string traits[numberOfNames] = { "Pyromancer","Butcher","Headhunter","Swift","Werewolf","Pirate","Necromancer","Druid","Crusher","Cannibal" };
string battlecries[numberOfNames] = { "You will perish in flames!","This will be a sloughter!","Your head will be my new troffic!","No one escape my arrows!"
,"Auuuuuuuuuuuuuuuu!","Arrrrrrrrrrrrrrrrrrrgh!","Your body will join to my collection!","I will show you the power of nature!","I will crush you!", "I will crash you!"};

inline string generateMaleName() {
	randActivate();
	return  maleNames[rand() % numberOfNames];
}

inline string generateFemaleName() {
	randActivate();
	return  femaleNames[rand() % numberOfNames];;
}

inline string generateSurname() {
	randActivate();
	return  surnames[rand() % numberOfNames];;
}

inline string generateTrait(string& battlecry ) {
	randActivate();
	int tmp = rand() % numberOfNames;
	battlecry = battlecries[tmp];
	return  traits[tmp];
}

bool isRandActive = false;
inline void randActivate() {
	if (isRandActive == 0) {
		srand(time(NULL));
		isRandActive = true;
	}
}