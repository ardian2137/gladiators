#include"Game.h"
#include"assets.h"
#include<cassert>

GameState mainMenuChoices[] = { GAMEMENU,LOADGAME,BESTRESULTS,END };
GameState gameMenuChoices[] = { ARENA,SAVEGAME,MARKET,MAINMENU };
GameState marketMenuChoices[] = { WEAPONSMITH,ARMORSMITH,WITCH,GAMEMENU };

Game::Game(void) {	
	
	loadTextures();
	playMusic();
	setTextOptions();
	placeComponents();

	gameState = MAINMENU;
}

Game::~Game(void) {}

void attack(Hero attacker,NPC defender) {
	defender.HP -=  (attacker.minDMG + attacker.maxDMG)/2;
}

void Game::run() { //game loop
	while (gameState != END)
	{
		switch (gameState)
		{
		case MAINMENU:
			mainMenu();
			break;
		case GAMEMENU:
			gameMenu();
			break;
		case ARENA:
			arena();
			break;
		case MARKET:
			marketMenu();
			break;
		case WEAPONSMITH:
			weaponsmith();
			break;
		case ARMORSMITH:
			armorsmith();
			break;
		case BESTRESULTS:
			bestResults();
			break;
		case WITCH:
			witch();
			break;
		case LOADGAME:
			gameState = GAMEMENU;
			break;
		case SAVEGAME:
			gameState = GAMEMENU;
			break;
		default:
			gameState = END;
		}
	}
	window.close();
}

void Game::mainMenu() {
	background.setTexture(mainMenuBackground);
	setTextPositions(mainMenuOptions);
	createMenu(MAINMENU, mainMenuChoices);
}

void Game::gameMenu() {
	background.setTexture(gameMenuBackground);
	setTextPositions(gameMenuOptions);
	createMenu(GAMEMENU, gameMenuChoices);
}

void Game::marketMenu() {
	background.setTexture(marketMenuBackground);
	setTextPositions(marketMenuOptions);
	createMenu(MARKET, marketMenuChoices);
}

void Game::arena() {
	placeEnemy();
	background.setTexture(arenaBackground);
	placeComeBackButton("SURRENDER!");
	createArenaLocation();
}

void Game::weaponsmith() {
	background.setTexture(weaponsmithBackground);
	placeComeBackButton("Comeback");
	createLocation(MARKET);
}

void Game::armorsmith() {
	background.setTexture(armorsmithBackground);
	placeComeBackButton("Comeback");
	createLocation(MARKET);
}

void Game::witch() {
	background.setTexture(witchBackground);
	placeComeBackButton("Comeback");
	createLocation(MARKET);
}

void Game::bestResults() {
	background.setTexture(bestResultsBackground);
	placeComeBackButton("Comeback");
	createLocation(MAINMENU);
}

void Game::createMenu(GameState currentState, GameState choices[]) {
	Text title("Gladiators", font, 120);
	if (gameState == MAINMENU) {
		title.setStyle(Text::Bold);

		title.setPosition(width / 2 - title.getGlobalBounds().width / 2, height - 700);
	}

	while (gameState == currentState)
	{
		Vector2f  mouse(Mouse::getPosition());

		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed || event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)
				gameState = END;
			for (int i = 0; i < numberOfOptions; i++) {
				if (options[i].getGlobalBounds().contains(mouse) && event.type == Event::MouseButtonReleased && event.key.code == Mouse::Left)
					gameState = choices[i];
			}

		}

		window.draw(background);
		if (gameState == MAINMENU)
			window.draw(title);
		
			for (int i = 0; i < numberOfOptions; i++)
				if (options[i].getGlobalBounds().contains(mouse))
					options[i].setFillColor(Color::Blue);
				else options[i].setFillColor(Color::White);

				for (int i = 0; i < numberOfOptions; i++)
					window.draw(options[i]);

		window.display();
	}		
}

void Game::createLocation(GameState previousGameState) {
	while (gameState != previousGameState)
	{
		Vector2f  mouse(Mouse::getPosition());

		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed || event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)
				gameState = END;

			if (comeBackButton.getGlobalBounds().contains(mouse) && event.type == Event::MouseButtonReleased && event.key.code == Mouse::Left)
				gameState = previousGameState;
		}
		window.draw(background);

		if (comeBackButton.getGlobalBounds().contains(mouse))
			comeBackButton.setFillColor(Color::Blue);
		else 
			comeBackButton.setFillColor(Color::White);

		window.draw(comeBackButton);		
		window.display();
	}
}

void Game::createArenaLocation() {
	while (gameState == ARENA)
	{
		if (player.HP <= 0 || rival.HP <= 0)
			gameState = GAMEMENU;
		Vector2f  mouse(Mouse::getPosition());

		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed || event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)
				gameState = END;

			if (comeBackButton.getGlobalBounds().contains(mouse) && event.type == Event::MouseButtonReleased && event.key.code == Mouse::Left)
				gameState = GAMEMENU;

			if (attackButton.getGlobalBounds().contains(mouse) && event.type == Event::MouseButtonReleased && event.key.code == Mouse::Left) {
				rival.HP -= (player.minDMG + player.maxDMG) / 2;// just for testing
				player.HP -= (rival.minDMG + rival.maxDMG) / 2;// just for testing
			}
		}

		window.draw(background);

		window.draw(attackButton);
		window.draw(hero);
		window.draw(enemy);

		if (comeBackButton.getGlobalBounds().contains(mouse))
			comeBackButton.setFillColor(Color::Blue);
		else 
			comeBackButton.setFillColor(Color::White);

		heroHealth.setString(to_string(player.HP)+ "/" + to_string(player.maxHP));
		enemyHealth.setString(to_string(rival.HP) + "/" + to_string(rival.maxHP));
		enemyName.setString(rival.name + " " + rival.getTrait());
		playerName.setString(player.name);

		window.draw(heroHealth);
		window.draw(enemyHealth);
		window.draw(enemyName);
		window.draw(playerName);

		window.draw(comeBackButton);		
		window.display();
	}
}