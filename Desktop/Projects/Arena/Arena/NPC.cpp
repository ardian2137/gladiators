#include"NPC.h"
#include<iostream>

using std::cout;
using std::endl;

NPC::NPC() {
	race = "Human";
	battlecry = "Auuuuuuu";
	name = "John";
	surname = "Lorentz";
	trait = "Werewolf";
	level = 1;
	HP = 50;
	maxHP = 50;
	minDMG = 1;
	maxDMG = 5;
	armor = 1;
	gold = 100;
	sex = male;
}

NPC::NPC(string Race, string Battlecry, string Name, int Level, int hp, int Armor, int MinDMG, int MaxDMG, int Gold,int Sex) {
	race = Race;
	battlecry = Battlecry;
	name = Name;
	level = Level;
	HP = hp;
	minDMG = MinDMG;
	maxDMG = MaxDMG;
	armor = Armor;
	gold = Gold;
	sex = Sex;
}

void NPC::meet() {
	cout << endl;
	cout << "You faced:" << name << endl;
	cout << name << ":" << battlecry << endl;
	cout << endl;
}

inline void NPC::description() {
	cout << endl;
	cout << "Name:" << name << endl;
	cout << "Race:" << race << endl;
	cout << "Gold:" << gold << endl;
	cout << "HP:" << HP << endl;
	cout << "Level:" << level << endl;
	cout << "Armor:" << armor << endl;
	cout << "Damage:" << minDMG << "-" << maxDMG << endl;
	cout << endl;
}

string NPC::getTrait() {
	return trait;
}
