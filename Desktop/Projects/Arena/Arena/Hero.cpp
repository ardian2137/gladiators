#include"Hero.h"
#include<iostream>
#include<string>
#include<fstream>

using std::string;
using std::endl;
using std::cout;

 Hero::Hero() {
	race = "";
	Class = "";
	level = 1;
	STR = 0;
	DEX = 0;
	INT = 0;
	HP = 100;
	maxHP = 100;
	EXP = 0;
	name = "Gladiator";
	armor = 0;
	minDMG = 1;
	maxDMG = 10;
	cloth = Cloth("cloth", 1.3, 23, 12);
	weapon = Weapon("dagger", 1.5, 3, 3, 5);
	gold = 250;
	mana = 0;
	stamina = 30;
	lvl_cup = 1000;
	look.loadFromFile("Resources/Img/hero.png");
}

inline void Hero::description() {
	cout << endl;
	cout << "Name:" << name << endl;
	cout << "Race:" << race << endl;
	cout << "Level:" << level<< endl;
	cout << "Experience:" << EXP << endl;
	cout << "Next level:" << lvl_cup << endl;
	cout << "Class:" << Class << endl;
	cout << "Gold:" << gold << endl;
	cout << "Health:" << HP << endl;
	cout << "Mana:" << mana << endl;
	cout << "Stamina:" << stamina << endl;
	cout << "Strength:" << STR << endl;
	cout << "Dexterity:" << DEX << endl;
	cout << "Inteligence:" << INT << endl;
	cout << "Cloth:" << cloth.name << endl;
	cout << "Weapon:" << weapon.name << endl;
	cout << "Armor:" << armor << endl;
	cout << "Damage:" << minDMG << "-" << maxDMG << endl;
}

inline void Hero::save() {
	std::ofstream file;
	file.open("D://save.txt");

	file << gold << endl;
	file << level << endl;
	file << race << endl;
	file << Class << endl;
	file << STR << endl;
	file << DEX << endl;
	file << INT << endl;
	file << EXP << endl;
	file << name << endl;
	file << armor << endl;
	file << minDMG << endl;
	file << maxDMG << endl;
	file << lvl_cup << endl;
	file << cloth.name << endl;
	file << cloth.value << endl;
	file << cloth.weight << endl;
	file << weapon.name << endl;
	file << weapon.value << endl;
	file << weapon.weight << endl;
	file << weapon.DMGmin << endl;
	file << weapon.DMGmax << endl;

	file.close();
	cout << endl;
	cout << "Game saved" << endl;
}

inline void Hero::load() {
	std::ifstream file;
	try
	{
		file.open("D://save.txt");
		file >> gold;
		file >> level;
		file >> race;
		file >> Class;
		file >> STR;
		file >> DEX;
		file >> INT;
		file >> EXP;
		file >> name;
		file >> armor;
		file >> minDMG;
		file >> maxDMG;
		file >> lvl_cup;
		file >> cloth.name;
		file >> cloth.value;
		file >> cloth.weight;
		file >> weapon.name;
		file >> weapon.value;
		file >> weapon.weight;
		file >> weapon.DMGmin;
		file >> weapon.DMGmax;

		HP = STR * 10;
		stamina = DEX * 10;
		mana = INT * 10;
	}
	catch (std::ifstream::failure e) {
		std::cerr << "Exception opening/reading/closing file\n";
	}
	file.close();
}

inline void Hero::setCloth(Cloth Cloth) {
	cloth = Cloth;
}

inline void Hero::setWeapon(Weapon Weapon) {
	weapon = Weapon;
}