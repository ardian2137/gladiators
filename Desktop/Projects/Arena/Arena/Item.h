#pragma once

#include<string>

class Item {
public:
	virtual void description() = 0;
	double weight;
	int value;
	std::string name;

};