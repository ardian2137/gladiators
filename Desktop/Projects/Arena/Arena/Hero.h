#pragma once

#include"character.h"
#include<string>
#include"Cloth.h"
#include"Weapon.h"
#include"NPC.h"

class Hero
	:public Character {
public:
	Hero();

	void description();

	void save();
	void load();

	void setCloth(Cloth cloth);
	void setWeapon(Weapon weapon);

private:
	string Class;
	int DEX, STR, INT, EXP, lvl_cup, mana, stamina;
	Cloth cloth;
	Weapon weapon;
};
