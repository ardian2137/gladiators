#include"Weapon.h"
#include<iostream>

using std::string;
using std::cout;
using std::endl;
using std::ostream;

Weapon::Weapon(string nam, double masa, int  val, int min, int max) {
	name = nam;
	weight = masa;
	value = val;
	DMGmin = min;
	DMGmax = max;
}

Weapon::Weapon() {
	DMGmin = 0;
	DMGmax = 0;
	value = 0;
	name = "";
	weight = 0;
}

void Weapon::description() {
	cout << endl;
	cout << "Name:" << name << endl;
	cout << "Weight:" << weight << endl;
	cout << "Value:" << value << endl;
	cout << "Damage:" << DMGmin << "-" << DMGmax << endl;
}
