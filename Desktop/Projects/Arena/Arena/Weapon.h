#pragma once

#include"Item.h"

class Weapon
	:public Item {
public:
	int DMGmin, DMGmax;
	Weapon(std::string nam, double masa, int val, int min, int max);
	Weapon();
	void description();
};
