#include"Cloth.h"
#include<iostream>

using std::string;
using std::cout;
using std::endl;
using std::ostream;

Cloth::Cloth(string nam, double masa, int val, int arm)
{
	name = nam;
	weight = masa;
	value = val;
	armor = arm;
}
Cloth::Cloth()
{
	armor = 0;
	value = 0;
	name = "";
	weight = 0;
}
void Cloth::description()
{
	cout << endl;
	cout << "Name:" << name << endl;
	cout << "Weight:" << weight << endl;
	cout << "Value:" << value << endl;
	cout << "Armor:" << armor << endl;

}
