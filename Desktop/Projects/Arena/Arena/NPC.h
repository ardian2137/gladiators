#pragma once

#include"Character.h"
#include"RNG.h"

using std::string;

class NPC
	:public Character {
private:
	string battlecry,trait;
public:
	string getTrait();
	void meet();
	void description();

	NPC(string Race, string Name, string Battlecry, int Level, int hp, int Armor, int MinDMG, int MaxDMG, int Gold = 0,int Sex = male);
	NPC();
};