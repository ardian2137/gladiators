#pragma once
#include<string>
#include <SFML\Graphics.hpp>

class Character {
public:
	enum {
		male,
		female
	};

	int maxHP, HP, armor,level, minDMG, maxDMG, gold, sex;
	std::string name, race, surname;
	sf::Texture look;
};
