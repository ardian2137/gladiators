#pragma once

#include <SFML\Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include "Hero.h"

using namespace std;
using namespace sf;
	
enum GameState { MAINMENU, GAMEMENU, GAME_OVER, END, LOADGAME, SAVEGAME, ARENA, BESTRESULTS, MARKET, WEAPONSMITH, ARMORSMITH ,WITCH};

class Game
{
public:
	Game(void);
	~Game(void);

	void run();

private:
	GameState gameState;
	Hero player;
	NPC rival;

	void mainMenu();
	void gameMenu();
	void marketMenu();
	void arena();
	void weaponsmith();
	void armorsmith();
	void witch();
	void bestResults();
	void createMenu(GameState state,GameState choices[]);
	void createLocation(GameState previousGameState);
	void createArenaLocation();

};
