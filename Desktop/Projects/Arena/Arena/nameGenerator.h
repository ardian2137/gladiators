#pragma once

#include<string>

using std::string;

string generateMaleName();
string generateFemaleName();
string generateSurname();