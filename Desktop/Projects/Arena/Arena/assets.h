#pragma once

#include <SFML\Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>

using namespace std;
using namespace sf;

const int width = 1280;
const int height = 720;

RenderWindow window(VideoMode(width, height), "Gladiators", Style::Fullscreen);

Texture mainMenuBackground, gameMenuBackground, marketMenuBackground, arenaBackground;
Texture weaponsmithBackground, armorsmithBackground, witchBackground, bestResultsBackground;
Texture heroPortrait, enemyPortrait,attackButtonTexture;

Font font;
Sprite background, hero, enemy, attackButton;
Music music;

const int numberOfOptions = 4;
Text options[numberOfOptions];
Text comeBackButton,heroHealth,enemyHealth,playerName,enemyName;
Event event;

string mainMenuOptions[] = { "New Game","Load game","Best results","Exit" };
string gameMenuOptions[] = { "Go to the arena","Save game","Go to the market","Go to the main menu" };
string marketMenuOptions[] = { "Go to the weaponsmith","go to the armorsmith","Go to the witch","Get back to the city center" };

void loadTextures() {
	mainMenuBackground.loadFromFile("Resources/img/Backgrounds/mainMenu.png");
	gameMenuBackground.loadFromFile("Resources/img/Backgrounds/gameMenu.png");
	marketMenuBackground.loadFromFile("Resources/img/Backgrounds/marketMenu.png");
	arenaBackground.loadFromFile("Resources/img/Backgrounds/arena.png");
	weaponsmithBackground.loadFromFile("Resources/img/Backgrounds/weaponsmith.png");
	armorsmithBackground.loadFromFile("Resources/img/Backgrounds/armorsmith.png");
	witchBackground.loadFromFile("Resources/img/Backgrounds/witch.png");
	bestResultsBackground.loadFromFile("Resources/img/Backgrounds/bestResults.png");
	heroPortrait.loadFromFile("Resources/img/Portraits/hero.png");
	enemyPortrait.loadFromFile("Resources/img/Portraits/werewolf.png");
	attackButtonTexture.loadFromFile("Resources/img/Buttons/attack.png");
}

void playMusic() {
	music.openFromFile("Resources/Music/theme.ogg");
	music.setLoop(true);
	music.play();
}

void setTextOptions() {
	heroHealth.setFont(font);
	heroHealth.setCharacterSize(40);

	enemyHealth.setFont(font);
	enemyHealth.setCharacterSize(40);

	comeBackButton.setFont(font);
	comeBackButton.setCharacterSize(60);

	for (int i = 0; i < numberOfOptions; i++)
	{
		options[i].setFont(font);
		options[i].setCharacterSize(60);
	}
	font.loadFromFile("Resources/Fonts/gladiators.ttf");
}

void placePlayer() {
	hero.setTexture(heroPortrait);
	hero.setPosition(50, 200);
}

void placeEnemy() {
	enemy.setTexture(enemyPortrait);
	enemy.setPosition(width - 310, 200);
}

void placeAttackButton() {
	attackButton.setTexture(attackButtonTexture);
	attackButton.setPosition(200, 200);
}

void placeComeBackButton(string text) {
	comeBackButton.setString(text);
	comeBackButton.setPosition(width - 250, 10);
}

void placeHeroHealth() {
	heroHealth.setFillColor(Color::Red);
	heroHealth.setPosition(150,150);
}

void placeEnemyHealth() {
	enemyHealth.setFillColor(Color::Red);
	enemyHealth.setPosition(width - 210, 150);
}

void placeEnemyName() {
	enemyName.setPosition(width - 260, 150);
}

void placeHeroName() {
	enemyName.setPosition(150, 100);
}

void setTextPositions(string optionsList[]) {
	for (int i = 0; i < numberOfOptions; i++)
	{
		options[i].setString(optionsList[i]);
		options[i].setPosition(width / 2 - options[i].getGlobalBounds().width / 2, height / numberOfOptions + i * height / (numberOfOptions * 2));
	}
}

void placeComponents(){
	placePlayer();
	placeAttackButton();
	placeHeroHealth();
	placeEnemyHealth();
	placeEnemyName();
	placeHeroName();
}